<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('statistics', 'StatisticsController@getClickInfo');
Route::get('statistics/tags', 'StatisticsController@getClickInfo');
Route::get('store/info', 'StorageController@manageInfo');
Route::get('get/info', 'StorageController@getInfo');
Route::get('test', 'PageController@test');
Route::get('get_url_info', 'PageController@sendUrlInfo');
Route::get('shorten_url', 'UrlController@sendHashCode');
Route::get('store_click_info', 'StorageController@storeUserAgent');
Route::get('get_security_code', 'SafetyController@sendSafetyCode');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});