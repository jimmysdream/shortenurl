export const poems = [
  {
    'title': '秋日赴闕题潼關驛樓',
    'text' : '紅葉晚蕭蕭，長亭酒一瓢。殘雲歸太華，疏雨過中條。樹色隨山迥，河聲入海遙。帝鄉明日到，猶自夢漁樵。',
    'width': 256
  },
  {
    'title': '香蓮碧水動風涼日月長',
    'text': '香蓮碧水動風涼, 水動風涼日月長, 長月日涼風動水, 涼風動水碧蓮香',
    'width': 307
  },
  {
    'title': '桃花庵歌',
    'text': '桃花塢裡桃花庵，桃花庵下桃花仙。 桃花仙人種桃樹，又摘桃花換酒錢。酒醒只在花前坐，酒醉還來花下眠。半醉半醒日復日，花落花開年復年。但願老死花酒間，不願鞠躬車馬前。車塵馬足顯者事，酒盞花枝隱士緣。若將顯者比隱士，一在平地一在天。若將花酒比車馬，彼何碌碌我何閒。別人笑我太瘋癲，我笑他人看不穿。不見五陵豪傑墓，無花無酒鋤作田。',
    'width': 328
  },
  {
    'title': '桃花詩',
    'text': '暮曉春來遲,先於百花知 歲歲種桃花,開在斷腸時',
    'width': 240
  },
  {
    'title': '詠梅',
    'text': '天嫌雪蒼白,信手綉梅花 來年冬日到,再與一處開',
    'width': 240
  },
  {
    'title': '詠梅',
    'text': '萬嶺千山攜白首,天賜胭脂輕抹腮 遙問蘭竹春何在,玉骨冰肌暗香來',
    'width': 307
  },
  {
    'title': '憶梅',
    'text': '定定住天涯,依依向物華 寒梅最堪恨,常作去年花',
    'width': 240
  },
  {
    'title': '詠杜鵑',
    'text': '春紅始謝又秋紅,息國亡來入楚宮 應是蜀冤啼不盡,更憑顏色訴秋風',
    'width': 307
  },
  {
    'title': '無名詩',
    'text': '我生君未生,君生我已老 雙枝併為春,歲歲做年少',
    'width': 240
  },
  {
    'title': '欲摘梨花',
    'text': '欲摘梨花新作酒,秦淮煙波雨不休 曉妝鏡前聽風雨,半遮臉來是離愁',
    'width': 307
  },
  {
    'title': '安碧如',
    'text': '白蓮聖母落紅塵,西微湖中嬉戲情 大漠千里隨君畔,苗寨守得明月心',
    'width': 307
  }
]