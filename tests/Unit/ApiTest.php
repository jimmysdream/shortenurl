<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiTest extends TestCase
{	
    /** @test */
    public function check_api_status()
    {	
		$get_url_info_response = $this->get('/get_url_info');
        $get_url_info_response->assertStatus(200);

        $get_security_code_response = $this->get('/get_security_code');
        $get_security_code_response->assertStatus(200);

        $shorten_url_response = $this->get('/shorten_url');
        $shorten_url_response->assertStatus(200);

        $store_click_info_response = $this->get('/store_click_info');
        $store_click_info_response->assertStatus(200);
	}
	
	// api/get_url_info
	/** @test */
	// public function send_correct_url_info_hash_exist_unsafe()
	// {
	// 	$response = $this->json('GET', '/api/get_url_info', ['hash' => 'jx9t386k']);
	// 	$response = json_decode($response->getContent(), true);
	// 	$data = [
	// 		'hash' => $response['hash'],
	// 		'url' => $response['url'],
	// 		'safe' => $response['safe']
	// 	];
	// 	$correct_data = [
	// 		'hash' => 'jx9t386k',
	// 		'url' => 'https://www.google.com',
	// 		'safe' => false
	// 	];
	// 	$this->assertEquals($data, $correct_data);
	// }

	/** @test */
	public function send_correct_url_info_hash_exist_safe()
	{
		$hash = 'jx9t3fu2';
		$response = $this->json('GET', '/api/get_url_info', ['hash' => $hash]);
		$response = json_decode($response->getContent(), true);
		$data = [
			'hash' => $response['hash'],
			'url' => $response['url'],
			'safe' => $response['safe']
		];
		$correct_data = [
			'hash' => $hash,
			'url' => 'https://www.google.com.tw',
			'safe' => true
		];
		$this->assertEquals($data, $correct_data);
	}

	/** @test */
	public function send_correct_url_info_hash_null()
	{
		$hash = null;
		$response = $this->json('GET', '/api/get_url_info', ['hash' => $hash]);
		$response = json_decode($response->getContent(), true);
		$data = [
			'hash' => $response['hash'],
			'url' => $response['url'],
			'safe' => $response['safe']
		];
		$correct_data = [
			'hash' => null,
			'url' => null,
			'safe' => null
		];
		$this->assertEquals($data, $correct_data);
	}

	/** @test */
	public function send_correct_url_info_hash_does_not_exist()
	{
		$hash = 'test';
		$response = $this->json('GET', '/api/get_url_info', ['hash' => $hash]);
		$response = json_decode($response->getContent(), true);
		$data = [
			'hash' => $response['hash'],
			'url' => $response['url'],
			'safe' => $response['safe']
		];
		$correct_data = [
			'hash' => $hash,
			'url' => null,
			'safe' => null
		];
		$this->assertEquals($data, $correct_data);
	}

    // api/get_security_code
    /** @test */
    // public function get_right_security_code_unsafe()
    // {
    //   $response = $this->json('GET', '/api/get_security_code', ['url' => 'https://www.google.com']);
    //   $safe = json_decode($response->getContent(), true)["safe"];
    //   $this->assertEquals($safe, false);
    // }

    /** @test */
    public function get_right_security_code_safe()
    {
      $response = $this->json('GET', '/api/get_security_code', ['url' => 'https://www.google.com.tw']);
      $safe = json_decode($response->getContent(), true)["safe"];
      $this->assertEquals($safe, true);
    }

    // api/shorten_url
    /** @test */
    public function send_right_hash_code()
    {
      $request = [
        'url' =>  "https://rhinoshield.tw/pages/personalization?_b=Apple&_m=iphone-xs&_c=SpaceGray&_pt=mod-nx-frame&_s=hotstamp&_bc=white&_bm=KRF16&utm_source=Facebook&utm_medium=Referral&utm_campaign=20190617_Facebook_kroma&fbclid=IwAR3FCY2BI7nQ2cFTbDycLz5uLuyvfRkC_2HE5Gl-0R3KlWBNfASFRe9CnD0",
        'hash' => "jxa5ajaq",
        'source' => "Facebook",
        'medium' => "Referral",
        'campaign' => "20190617_Facebook_kroma",
        'term' => "0",
        'content' => "0"
      ];
      $response = $this->json('GET', '/api/shorten_url', $request);
      $hash = json_decode($response->getContent(), true)["hash"];
      $this->assertEquals($hash, "jx9s2qij");
    }
}
