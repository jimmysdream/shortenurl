<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Services\SafetyService;

use Redis;

class SafetyTest extends TestCase
{
    /** @test */
    // public function get_correct_safety_code_unsafe()
    // {
    //   $safetyService = new SafetyService();
    //   $originUrl = "https://www.google.com";
    //   $safe = $safetyService->webIsSafe($originUrl);

    //   $this->assertEquals($safe, false);
    // }

    /** @test */
    public function get_correct_safety_code_safe()
    {
      $safetyService = new SafetyService();
      $originUrl = "https://www.google.com.tw";
      $safe = $safetyService->webIsSafe($originUrl);

      $this->assertEquals($safe, true);
    }
}
