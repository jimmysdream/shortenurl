<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Services\StorageService;
use App\Services\UrlService;

use Redis;

class StorageTest extends TestCase
{
    /** @test */
    public function correctly_store_data()
    {
      $storageService = new StorageService();
      $urlService = new UrlService($storageService);
      $url = "https://www.hello.com";
      $hash = "test";
      $source = "test";
      $medium = "test";
      $campaign = "test";
      $term = "test";
      $content = "test";

      $hash = 'jx9t3fu2';
      $response = $this->json('GET', '/api/shorten_url', 
      [
        'url' => $url,
        'hash' => $hash,
        'source' => $source,
        'medium' => $medium,
        'campaign' => $campaign,
        'term' => $term,
        'content' => $content
      ]);
      $response = json_decode($response->getContent(), true);
      $this->assertDatabaseHas('basic_url_data', [
        'origin_url' => $url
      ]);
    }
}
