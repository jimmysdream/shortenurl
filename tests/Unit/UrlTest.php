<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Services\UrlService;
use App\Services\StorageService;

use Redis;

class UrlTest extends TestCase
{
  private $redis_ip = "192.168.32.4";

    /** @test */
    public function use_right_redis_ip()
    {
      $correct_redis_ip = $this->redis_ip;
      $storageService = new StorageService();
      $urlService = new UrlService($storageService);
      $current_redis_ip = $urlService->redis_ip;
      $this->assertEquals($correct_redis_ip, $current_redis_ip);
    }

    /** @test */
    public function correctly_get_hash()
    {
      $storageService = new StorageService();
      $urlService = new UrlService($storageService);
      $exist_hash = 'jx9t386k';
      $new_hash = $urlService->getNewHashCode($exist_hash);
      $this->assertNotEquals($exist_hash, $new_hash);
    }
}
