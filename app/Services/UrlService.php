<?php

namespace App\Services;

use phpDocumentor\Reflection\Types\Boolean;
use PhpParser\Node\Scalar\String_;

use Redis;
use Illuminate\Http\Request;

use App\Services\StorageService;

class UrlService
{
    public $redis_ip;
    private $storageService;

    public function __construct(StorageService $storageService)
    {
        $this->storageService = $storageService;
    }

    /**
     * Store the shorten url to redis
     * key: original url
     * value: hash
     *
     * @return
     */
    public function cacheOriginUrl(string $url, string $hash)
    {
        Redis::select('0');
        Redis::set($url, $hash);

        return;
    }

    /**
     * Get original url
     *
     * @return string|null
     */

    public function getOriginUrl(string $hash)
    {
        Redis::select('1');
        $url = Redis::get($hash);
        if (!$url) {
            $url = null;
        }
        return $url;
    }

    /**
     * Store the hash code to redis
     * key: hash code
     * value: original url
     *
     * @return
     */
    public function cacheHashCode(string $hash, string $url)
    {
        Redis::select('1');
        Redis::set($hash, $url);
    }

    /**
     * Check if the url already has corresponding hash code
     * return hash code or "" (if not exists)
     *
     * @return string|null
     */

    public function getHashCode(string $url)
    {
        Redis::select('0');
        $hash = Redis::get($url);
        if ($hash !== false) {
            return $hash;
        }
        return null;
    }

    /**
     * Check if the hash code has been used
     * return available hash code
     *
     * @return string
     */
    public function getNewHashCode(string $hash)
    {
        Redis::select('1');
        $url = Redis::get($hash);

        // if hash code has been used
        if ($url !== false && $url !== null) {
            $hash = $this->generateRandom(6);
            $hash = $this->getNewHashCode($hash);
        }
        return $hash;
    }

    /**
     * delete redis url and hash
     *
     * @return
     */
    public function delCache(string $url)
    {
        $hash = Redis::get($url);
        Redis::del($url);
        Redis::select("1");
        Redis::del($hash);
        return;
    }

    /**
     * Generate random string
     *
     * @return string
     */

    public function generateRandom(int $int)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $int; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * transform url to hash code
     *
     * @return string
     */
    public function hashing(array $urlInfo)
    {
        $hash = $this->getHashCode($urlInfo["url"]); // check if the url already has corresponding hash code

        if ($hash) {
            return $hash;
        }

        $newHash = $this->getNewHashCode($urlInfo["hash"]); // get new hash code when old hash code has been used
        $this->cacheOriginUrl($urlInfo["url"], $newHash);
        $this->cacheHashCode($newHash, $urlInfo["url"]);
        $this->storageService->storeUrlParams($urlInfo["url"], $newHash, $urlInfo["source"], $urlInfo["medium"], $urlInfo["campaign"], $urlInfo["term"], $urlInfo["content"]);
        return $newHash;
    }
}



