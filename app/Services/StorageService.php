<?php

namespace App\Services;

use phpDocumentor\Reflection\Types\Boolean;
use PhpParser\Node\Scalar\String_;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\BasicUrlData;
use App\IncomeSource;

use Illuminate\Http\Request;

use GuzzleHttp\Client;

class StorageService
{

    /**
     * Store the information of url into mysql
     *
     * @return void
     */
    public function storeUrlParams(string $url, string $hash, string $source, string $medium, string $campaign, string $term, string $content)
    {
        BasicUrlData::create([
          'origin_url'=> $url,
          'hash_code'=> $hash,
          'full_link'=> 'https://briefy.it/'.$hash,
          'utm_source'=> $source,
          'utm_medium'=> $medium,
          'utm_campaign'=> $campaign,
          'utm_term'=> $term,
          'utm_content'=> $content
        ]);
    }

    /**
     * Store the click info from client
     *
     * @param request
     * @param hash_code
     *
     * @return void
     */
    public function storeUserAgent(Request $request, string $hash_code)
    {
        // $forwarded_ip = $request->server()['HTTP_X_FORWARDED_FOR'] ? $request->server()['HTTP_X_FORWARDED_FOR'] : null;
        $remote_ip = $request->server()['REMOTE_ADDR'] ? $request->server()['REMOTE_ADDR'] : null;
        $user_ip = $remote_ip;

        $time = (String) Carbon::now();
        $id = BasicUrlData::where('hash_code', $hash_code)->first()->id;
        $userForms = $request->header()['user-agent'][0];
        $index = strpos($userForms, ')');
        $userDevice = substr($userForms, 0, $index+1);
        $userBrowser = substr($userForms, $index+1, strlen($userForms));
        IncomeSource::create([
            'basic_url_data_id'=> $id,
            'device'=> $userDevice,
            'browser'=> $userBrowser,
            'click_time'=> $time,
            'user_ip' => $user_ip,
        ]);
    }

    /**
     * Store the tags into basic_url_data
     * @param hash_code
     * @param tags
     * @param name
     *
     * @return
     */
    public function manageInfo(string $hash_code, string $tags, string $name)
    {
        $origin_data = BasicUrlData::where('hash_code', $hash_code);
        $origin_data->update(['tags' => $tags, 'customized_name' => $name]);
    }

    /**
     * Get the tags from basic_url_data
     * @param hash_code
     *
     * @return tags
     */

    public function getInfo(string $hash_code)
    {
        $info = BasicUrlData::where('hash_code', $hash_code)->first();
        $tags = $info->tags;
        $name = $info->customized_name;
        return ['tags' => $tags, 'name' => $name];
    }

    /**
     * @param hash_code
     *
     * @return url
     */

    public function getRedirectUrl(string $hash_code)
    {
        $info = BasicUrlData::where('hash_code', $hash_code)->first();
        if (is_null($info)){
            return '';
        }
        return $info->origin_url;
    }
}



