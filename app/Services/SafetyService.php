<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Boolean;
use PhpParser\Node\Scalar\String_;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class SafetyService
{
    public function __construct()
    {
      $this->connection = new Client();
    }

    /**
     * Check the redirect web page safe or not
     * 
     * @return boolean
     */
    public function webIsSafe(string $originUrl)
    {
        $url = 'https://www.virustotal.com/vtapi/v2/url/report?apikey=43766532f74a597edafe9bf0841985a43107922ee9df7207103b60845836db87&resource='.$originUrl."&scan=1";
        $response = $this->connection->request('GET', $url);
        $status = json_decode($response->getBody()->getContents(), true);
        $response_code = $status['response_code'];

        $web_does_not_exist = ($response_code === 0 || $response_code === -2);
        $web_is_unsafe = ($response_code === 1 && $status['positives'] === 1);

        if ($web_does_not_exist || $web_is_unsafe) {
            return false;
        }
        return true;

    }
}