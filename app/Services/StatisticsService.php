<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Boolean;
use PhpParser\Node\Scalar\String_;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\BasicUrlData;
use App\IncomeSource;

class StatisticsService
{
    public function __construct()
    {
      $this->connection = new Client();
    }

    /**
     * Get the click information for input url
     *
     * @return Array
     */
    public function getClickInfo(string $name, string $country)
    {
      $result = [];
      $infos = BasicUrlData::where([['customized_name', 'like', '%'.$name.'%'], ['tags', 'like', '%'.$country.'%']])->get()->toArray();
      if (isset($infos)){
        foreach($infos as $info)
        {
          $data = IncomeSource::where('basic_url_data_id', $info['id'])->select('device', 'browser', 'click_time')->orderBy('click_time', 'desc')->get()->toArray();
          array_push($data, $info['full_link']);
          array_push($data, $info['customized_name']);
          array_push($data, $info['origin_url']);
          array_push($result, $data);
        }
        return $result;
      }
      return false;
    }
    public function getClickInfoFromTags(array $tags, string $country)
    {
      $result = [];
      $condtion = [];
      foreach($tags as $tag)
      {
          array_push($condtion, ['tags', 'like', '%'.$tag.'%']);
      }
      array_push($condtion, ['tags', 'like', '%'.$country.'%']);
      $datas = BasicUrlData::where($condtion)
                            ->select('id', 'full_link', 'customized_name', 'origin_url')
                            ->get()
                            ->toArray();
      foreach($datas as $info)
      {
          $data = IncomeSource::where('basic_url_data_id', $info['id'])->select('device', 'browser', 'click_time')->get()->toArray();
          array_push($data, $info['full_link']);
          array_push($data, $info['customized_name']);
          array_push($data, $info['origin_url']);
          array_push($result, $data);
      }
      return $result;
    }
}