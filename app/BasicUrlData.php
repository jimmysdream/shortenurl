<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BasicUrlData extends Model
{
    protected $table = 'basic_url_data';
    protected $fillable = [
        'origin_url',
        'hash_code',
        'full_link',
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_term',
        'utm_content',
        'tags',
        'customized_name'
    ];
}
