<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UrlService;
use Illuminate\Support\Facades\DB;

class UrlController extends Controller
{
    private $urlService;

    public function __construct(UrlService $urlService)
    {
        $this->urlService = $urlService;
    }

    public function sendHashCode(Request $request)
    {
        $hash = $this->urlService->hashing($request->toArray());
        return response()->json(["hash" => $hash]);
    }
}
