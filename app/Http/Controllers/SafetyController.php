<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SafetyService;

class SafetyController extends Controller
{
    private $checksafetyService;

    public function __construct(SafetyService $safetyService)
    {
        $this->safetyService = $safetyService;
    }

    /**
     * check if the website is safe, and send to client
     *
     * @param request
     * 
     * @return number(0:safe/1:unsafe)
     */
    public function sendSafetyCode(Request $request)
    {
        $url = trim($request->url);
        $safe = $this->safetyService->webIsSafe($url);
        return response()->json(['safe' => $safe]);
    }
}
