<?php

namespace App\Http\Controllers;


use App\Services\StatisticsService;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    private $statisticsService;

    public function __construct(StatisticsService $statisticsService)
    {
        $this->statisticsService = $statisticsService;
    }

    public function getClickInfo(Request $request)
    {
        $name = trim($request->name);
        $tags = $request->tags;
        $country = empty($request->country) ? '' : $request->country;
        if (empty($tags)){
            $result = $this->statisticsService->getClickInfo($name, $country);
        }
        if (empty($name)){
            $result = $this->statisticsService->getClickInfoFromTags($tags, $country);
        }

        return response()->json($result);
    }
}
