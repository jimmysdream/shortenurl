<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UrlService;
use App\Services\SafetyService;
use App\Services\StorageService;

class PageController extends Controller
{
    protected $urlService;
    protected $safetyService;

    public function __construct(UrlService $urlService, SafetyService $safetyService, StorageService $storageService)
    {
        $this->urlService = $urlService;
        $this->safetyService = $safetyService;
        $this->storageService = $storageService;
    }

    /**
     * Send safety_code to client to decide which page to shown
     *
     * @return view
     */
    public function main(Request $request, $any='')
    {
        if ($any){
            $url = $this->storageService->getRedirectUrl($any);
            if ($url !== '') {
                $this->storageService->storeUserAgent($request, $any);
                return redirect($url);
            }
            return view("main");
        }
        return view("main");
    }

    /**
     * Send url info to client
     */
    public function sendUrlInfo(Request $request)
    {
        $hash = $request->hash;
        $url = null;
        $safe = null;

        if ($hash) {
            $url = $this->urlService->getOriginUrl($hash);
            if ($url) {
                $safe = $this->safetyService->webIsSafe($url);
                $this->storageService->storeUserAgent($request, $hash);
            }
        }

        return response()->json(["hash" => $hash, "url" => $url, "safe" => $safe]);
    }

    public function test()
    {
        $this->safetyService->checkWebSafety('https://lumen.laravel.com/docs/5.8/routing');
    }

}