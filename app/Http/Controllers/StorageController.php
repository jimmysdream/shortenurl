<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\StorageService;

class StorageController extends Controller
{
    private $storageService;

    public function __construct(StorageService $storageService)
    {
        $this->storageService = $storageService;
    }

    /**
     * store click information to database
     *
     * @param request
     *
     */
    public function storeUserAgent(Request $request)
    {
        $hash = trim($request->hash);
        $this->storageService->storeUserAgent($request, $hash);
        return;
    }

    /**
     * store the tags information into basic_url_data table
     * @param request
     */
    public function manageInfo(Request $request)
    {
        $hash_code = trim($request->hash);
        $tags = trim($request->tags);
        $name = trim($request->name);
        $this->storageService->manageInfo($hash_code, $tags, $name);
        return;
    }

    /**
     * Get the tags information from basic_url_data table
     * @param request
     *
     * @return tags
     */
    public function getInfo(Request $request)
    {
        $hash_code = trim($request->hash);
        $result = $this->storageService->getInfo($hash_code);
        return $result;
    }
}
