<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IncomeSource extends Model
{
    protected $table = 'income_source';

    protected $fillable = [
        'basic_url_data_id',
        'device',
        'browser',
        'click_time',
        'user_ip'
    ];
}
