<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomeSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('income_source', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->integer('basic_url_data_id')->unsigned();
            $table->foreign('basic_url_data_id')->references('id')->on('basic_url_data')->onDelete('cascade')->onUpdate('cascade');
            $table->string('device');
            $table->string('browser');
            $table->timestamp('click_time');
            $table->string('user_ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('income_source');
    }
}
