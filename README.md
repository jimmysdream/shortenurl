# Briefy It

## Description
shorten url and store click information

## Structure
laravel + vue + mysql

### laravel
#### Home: deal with pages presented
dir: app/Services/
* HomeController
* HomeService

#### Url: convert original url to shortened url
dir: app/Http/Controllers
* UrlController
* UrlService

### Vue
#### App
dir: resources/client/src/main.vue
#### shorten
dir: resources/client/src/components/shorten.vue

### API
VirusTotal: https://developers.virustotal.com/reference#public-vs-private-api
```/url/report```

### response
* response_code: 0(data not exists in VirusTotal dataset), 1(data exists), -2(requested item is still queued for analysis)
* positives: 0(safe website), 1(unsafe website)
